from oom import exit_on_out_of_ram

one_gigabyte = 1 << 30
exit_on_out_of_ram(
    terminate_on=one_gigabyte,
    warn_on=2 * one_gigabyte,
    sleep_time=1,
    notify_about_using=True
)

# explode your RAM
extremely_big_number = 1 << 9999999
_ = [i for i in range(extremely_big_number)]
